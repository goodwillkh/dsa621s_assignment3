import ballerinax/kafka;
import ballerina/graphql;
import ballerina/io;
import ballerina/docker;
 
 
@docker:Config {
  name:"managerAPIv1",
  tag:"v1.0"
}

kafka:Producer students = check new (studentSign);
kafka:Producer headOfDepartment = check new (CreateCourse);
kafka:Producer lectures = check new (LectureContent);



@kubernetes:Deployment { image:"consumer-service", name:"kafka-consumer" }
kafka:ProducerConfiguration studentSign = {
	bootstrapServers: "localhost:9092",
	clientId: "student-Sign",
	acks: "all",
	retryCount: 3
};

kafka:ProducerConfiguration CreateCourse = {
	bootstrapServers: "localhost:9092",
	clientId: "Create-Course",
	acks: "all",
	retryCount: 3
};

kafka:ProducerConfiguration LectureContent = {
	bootstrapServers: "localhost:9092",
	clientId: "Lecture-Content",
	acks: "all",
	retryCount: 3
};




// must be http service
service graphql:Service /graphql on new graphql:Listener(9090) {
 
 //Get Student Details Studetn
 
 // gets strings return a json record
    isolated resource function get studentSig(string name,int studentid,string signature) returns string {
            Student astudeet = {name,studentid,signature};
            studentDigitalSig[studentid.toString()] = {name:name,sID:studentid,party:signature};

            io:println(studentDigitalSig);
			
			SignedCourse final = checkpanic courseDetails.mergeJson(astudeet);
        return "Candidate registered succesfully : " + name;
    }
    
    
    isolated resource function get CreateCourse(json data) returns string {
            CreatedCourse course_info = data ;

            byte[] serialisedMsg = course_info.toString().toBytes();
											// sends the record here
				// to send to kafka
              checkpanic getCourseInfro->sendProducerRecord({
                                    topic: "courseInformation",
                                    value: serialisedMsg });

             io:println(getCourseInfro);
			 return "the course added";
    }


    isolated resource function get completeCourse(json courseInfo) returns string { // register as a voter
										  // Must merge two jsons here the course data and course information  json j8 = checkpanic j5.mergeJson(j7);
			Courses course_info ={courseInfo};
            // 
            	
			byte[] serialisedMsg2 = course_info.toString().toBytes();
			//to send to kafka
			checkpanic getCourseInfro->sendProducerRecord({
                                    topic: "coursecreated",
                                    value: serialisedMsg2 });

            io:println(course_info.toString());
        return "Courses of the Lecture, ";
    }
   
    isolated    resource function post CourseOutlin() returns json { // the completed course
										  
            	
			byte[] serialisedMsg3 = courseDetails.toString().toBytes();
			//to send to kafka
			checkpanic getCourseInfro->sendProducerRecord({
                                    topic: "courseoutline",
                                    value: serialisedMsg3 });

            io:println(course_info.toString());
        return "Courses of the Lecture, ";
    }

}


//records
public type Student record {
    string name;
    int studentid;
    string signature;
};


public type CreatedCourse record {
    json data;
    
};


public type Courses record {
    json details;
};

//Course details maps
map<json> studentDigitalSig ={};
map<json> courseDetails ={};
map<json> SignedCourse ={};
